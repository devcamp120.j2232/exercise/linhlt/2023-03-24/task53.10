package s10;

public class Circle {
    private double radius = 1.0;
    //khởi tạo không tham số
    public Circle() {
    }
    //khởi tạo có tham số
    public Circle(double radius) {
        this.radius = radius;
    }
    //getter and setter
    public double getRadius() {
        return radius;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }
    public double getArea(){
        return Math.pow(this.radius,2) * Math.PI;
    }
    public double getCircumference(){
        return 2 * this.radius * Math.PI;
    }
    @Override
    public String toString() {
        return "Circle [radius=" + radius + "]";
    }
    
}
