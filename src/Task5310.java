import s10.Circle;
public class Task5310 {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3.0);
        System.out.println(circle1.getRadius());
        System.out.println(circle1.getArea());
        System.out.println(circle1.getCircumference());
        System.out.println(circle2);
        System.out.println(circle2.getArea());
        System.out.println(circle2.getCircumference());
    }
}
